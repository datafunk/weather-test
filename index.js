#! /usr/local/bin/node

/* istanbul ignore else  */
if ( process.env.NODE_ENV === 'test' ) {
    test = true
} else {
    test = false
}

const fs = require( 'fs' )
const prompt = require( 'prompt' )
const moment = require( 'moment' )

var weather = {}
module.exports = weather

weather.database = {}
weather.output = ''

weather.user_input = user_input = {}
weather.input_schema = input_schema = {
    properties: {
        city: {
            required: true,
            message: 'select from above',
            type: 'string'
        },
        date: {
            required: true,
            message: 'yyyymmdd or yyyy-mm-dd',
            pattern: /[1-2]{1}[0-9]{3}[-]*[0-9]{2}[-]*[0-9]{2}[-]*/,
            type: 'string',
        }
    }
}

weather.capitalise = capitalise = function ( string ) {
    return string.charAt( 0 ).toUpperCase() + string.slice( 1 ).toLowerCase()
}

weather.getData = function ( callback ) {
    fs.readFile( './data/extremes.json', 'utf-8', function ( err, data ) {
        if ( err ) throw error

        db = JSON.parse( data )
        weather.database = db
        callback()
    } )
}

weather.instruct = function () {
    db = weather.database

    
    if ( !test ) {
        console.log( 'Select a city and provide a date (yyyymmdd) to get weather report' )
        console.log( 'Available locations:\n' )

        for ( key in db ) {
            console.log( '\t' + key )
        }



        ( function getInput() {
            prompt.start()
            prompt.get( input_schema, function ( err, result ) {
                if ( err ) throw err

                var valid_city = 0
                var valid_date = 0

                var city = capitalise( result.city )

                if ( db[ city ] !== undefined ) {
                    valid_city = 1
                    user_input.city = city
                } else {
                    console.log( red( 'Invalid city input' ) )
                    user_input.city = null
                    getInput()
                }

                var date = result.date
                var date_rgx = /[1-2]{1}[0-9]{3}[-]*[0-9]{2}[-]*[0-9]{2}[-]*/

                if ( date.match( date_rgx ) ) {
                    valid_date = 1
                    user_input.date = moment( result.date ).toISOString()
                    user_input.month = moment( result.date ).format( 'MMM' )
                } else {
                    console.log( red( 'Invalid date input' ) )
                    console.log( red( 'Date should be yyyy-mm-dd or yyyymmdd' ) )
                    user_input.date = null
                    getInput()
                }

                if ( valid_city && valid_date ) {
                    weather.generate( user_input )
                }

            } )

        } )()



    }
}

weather.getData( weather.instruct )


weather.generate = function ( user_input ) {

    var db = weather.database
    max_c = db[ user_input.city ].extreme[ user_input.month ][ 0 ]
    min_c = db[ user_input.city ].extreme[ user_input.month ][ 1 ]
    rain_ = db[ user_input.city ].extreme[ user_input.month ][ 2 ]

    var rand = ( function ( min, max ) {
        return Math.random() * ( max - min ) + min
    } )

    var celsius = rand( min_c, max_c )
    var temp = ( function () {
        if ( celsius > 0 ) {
            t = '+' + celsius.toFixed( 1 )
        } else if ( celsius < 0 ) {
            t = celsius.toFixed( 1 )
        }
        return t
    } )

    var precipitation = rand( 0, rain_ ).toFixed( 0 )

    var humidity = 0

    var condition = ( function () {
        if ( precipitation > rain_ / 2 && celsius > 0 ) {
            humidity = rand( 30, 100 ).toFixed( 0 )
            cond = 'Rain'
        } else if ( precipitation > rain_ / 4 && celsius <= 0 ) {
            humidity = rand( 0, 10 ).toFixed( 0 )
            cond = 'Snow'
        } else {
            if ( celsius < 10 ) {
                humidity = rand( 0, 40 ).toFixed( 0 )
            } else {
                humidity = rand( 40, 100 ).toFixed( 0 )
            }
            cond = 'Sunny'
        }
        return cond
    } )

    var pressure = rand( 980.01, 1025.99 ).toFixed( 1 )

    weather.output = user_input.city
    weather.output += '|' + db[ user_input.city ].lat.toFixed( 2 )
    weather.output += ',' + db[ user_input.city ].lng.toFixed( 2 )
    weather.output += ',' + db[ user_input.city ].ele.toFixed( 2 )
    weather.output += '|' + user_input.date
    weather.output += '|' + condition()
    weather.output += '|' + temp()
    weather.output += '|' + pressure
    weather.output += '|' + humidity

    /* istanbul ignore next */
    if ( !test ) {
        console.log( weather.output )

        prompt.get( {
            properties: {
                repeat: {
                    message: 'Run same query again? [Y/n]'
                }
            }
        }, function ( err, result ) {
            if ( result.repeat.toLowerCase() !== 'n' ) {
                weather.generate( user_input )
            } else {
                console.log( 'Good bye!' )
            }
        } )
    }
}


function red( s ) {
    return '\033[31m' + s;
}

function onPromptErr( err ) {
    console.log( red( err ) )
        // return 1
}
