# Weather Data - test CLI application
This project was developed and run on node v6.4.0
Use __nvm__ to switch between node versions, if need be.

## How to use

1. Clone the repo
2. ```npm install```
3. ```npm start```
4. Follow input instructions

## Test

Test with mocha + chai
Test coverage with istanbul (and yes it could be improved, but ran out of time)

To run test just ```npm test```

## Specs
See ```Challenge.md```


## Notes

I chose to write the solution in node.js to address feedback from our initial meeting regarding my backend capabilities. The current solution - time permitting - could be quickly and easily expanded to

- take CLI arguments as input
- extending it with express (or similar) to transform it into a web service

To generate reasonably realistic (fake) weather data respective to location and seasonal changes, I used sample data from various stations as available at http://www.weatherzone.com.au/climate/stationdrill.jsp

I have several ideas on how to improve and extend the application, however I was mindful to stay within the suggested timeframe.
