#! /usr/local/bin/node

process.NODE_ENV = 'test'

const moment = require( 'moment' )
const assert = require( "assert" )
const chai = require( 'chai' )
const expect = require( 'chai' ).expect
const should = require( 'chai' ).should()

var weather = require( "../index.js" )

describe( 'weather', function () {

    before( function () {
        weather.getData( weather.instruct )
        user_input.city = weather.capitalise( 'sYdney' )
        user_input.date = moment( '20110728' ).toISOString()
        user_input.month = moment( user_input.date ).format(
            'MMM' )
    } )

    describe( 'input_schema.input_schema.properties.date.pattern', function () {
        it( 'validates date input with dash', function () {
            expect( '2016-11-14' ).to.match( weather.input_schema
                .properties.date.pattern )
        } )
        it( 'validates date input w/out dash', function () {
            expect( '20161114' ).to.match( weather.input_schema
                .properties.date.pattern )
        } )
    } )

    describe( 'weather', function () {
        it( 'is an object', function () {
            expect( weather ).to.be.a( 'object' )
        } )
    } )

    describe( '.getData populates .database', function () {
        it(
            '.database should eventually have a property `Sydney`',
            function () {
                expect( weather.database ).to.have.property(
                    'Sydney' )
            } )
    } )

    describe( '.capitalise converts input strings to match data format', function () {
        it(
            'should convert user input to correct capitalisation',
            function () {
                expect( user_input.city ).to.equal(
                    'Sydney' )
            } )
    } )

    describe( '.instruct takes user input', function () {
        it( 'calls .generate function once fetched valid user_input',
            function () {
                weather.generate( user_input )
            } )
    } )

    describe( '.generate processes database based on user_input', function () {
        describe(
            'min_c to be the low temperature mark for said city and month',
            function () {
                it( 'should be 2.2 degrees celsius',
                    function () {
                        expect( weather.database[
                            'Sydney' ].extreme[
                            'Jul' ][ 1 ] ).to.equal(
                            2.2 )
                    } )
            } )

        describe( 'min_c should be less than max_c', function () {
            it( 'must return true', function () {
                expect( weather.database[
                    'Sydney' ].extreme[
                    'Jul' ][ 1 ] ).to.be.below(
                    weather.database[
                        'Sydney' ].extreme[
                        'Jul' ][ 0 ] )
            } )
        } )

        describe(
            'temp is celsius converted to string and prefixed when positive',
            function () {
                before( function () {
                    var celsius_1 = 2.0
                    var celsius_2 = -2.0
                    temp_1 = '+' + celsius_1.toFixed(
                        1 )
                    temp_2 = celsius_2.toFixed( 1 )
                } )
                it(
                    'prefixes positive number inputs with `+`',
                    function () {
                        expect( temp_1 ).to.equal(
                            '+2.0' )
                    } )
                it(
                    'converts negative number inputs to string',
                    function () {
                        expect( temp_2 ).to.equal(
                            '-2.0' )
                    } )
            } )

        describe(
            'condition evaluates precipitation, temperature returns condition and humidity',
            function () {
                it( 'returns the weather condition',
                    function () {
                        expect( cond ).to.be.oneOf( [
                                'Snow', 'Rain',
                                'Sunny' ] )
                    } )
            } )

        describe( '.output is a strictly structured string', function () {
            before( function () {
                var sample_out =
                    'Coffs Harbour|-33.86,151.21,39|2015-12-23T05:02:12Z|Rain|+12.5|1004.3|97'
                sample_arr = sample_out.split( '|' )
                rgx_0 = /[a-zA-Z\s]{3,}/
                rgx_1 = /[-]*[0-9]{1,3}\.[0-9]*\,[-]*[0-9]{1,3}\.[0-9]*\,[0-9]{1,}/
                rgx_2 =
                    /[0-9]{4}[-]*[0-9]{2}[-]*[0-9]{2}T[0-9]{2}\:[0-9]{2}\:[0-9]{2}Z/
                rgx_3 = /[a-zA-Z]{4,5}/
                rgx_4 = /[\+\-]{1}[0-9]{1,2}\.[0-9]{1}/
                rgx_5 = /[0-9]{3,4}\.[0-9]{1}/
                rgx_6 = /[0-9]{1,3}/
            } )

            it( 'should match regex 0', function () {
                expect( sample_arr[ 0 ] ).to.match(
                    rgx_0 )
            } )

            it( 'should match regex 1', function () {
                expect( sample_arr[ 1 ] ).to.match(
                    rgx_1 )
            } )

            it( 'should match regex 2', function () {
                expect( sample_arr[ 2 ] ).to.match(
                    rgx_2 )
            } )

            it( 'should match regex 3', function () {
                expect( sample_arr[ 3 ] ).to.match(
                    rgx_3 )
            } )

            it( 'should match regex 4', function () {
                expect( sample_arr[ 4 ] ).to.match(
                    rgx_4 )
            } )

            it( 'should match regex 5', function () {
                expect( sample_arr[ 5 ] ).to.match(
                    rgx_5 )
            } )

            it( 'should match regex 6', function () {
                expect( sample_arr[ 6 ] ).to.match(
                    rgx_6 )
            } )

        } )
    } )

} )
